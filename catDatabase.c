///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file catDatabase.c 
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   08_03_2022
///////////////////////////////////////////////////////////////////////////////

#include "config.h"
#include "catDatabase.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int numCats;
struct Cat cat[MAX_CATS];

void initializeDatabase() {
   numCats = 0;
   for(int i = 0; i<= MAX_CATS; i++){
      strcpy(cat[i].name, "-");
      cat[i].isFixed = false;
      cat[i].weight = 0;
      cat[i].catGender = UNKNOWN_GENDER;
      cat[i].catBreed = UNKNOWN_BREED;
      cat[i].collarColor1 = UNKNOWN_COLOR;
      cat[i].collarColor2 = UNKNOWN_COLOR;
      cat[i].license = 0;
   }
}
