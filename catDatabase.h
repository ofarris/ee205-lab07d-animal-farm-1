///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file catDatabase.h
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   08_03_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>
#define MAX_CATS 1024
#define MAX_CAT_NAME 50

enum gender{UNKNOWN_GENDER,MALE,FEMALE};
enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
enum Color{BLACK, WHITE, RED, BLUE, GREEN, PINK, UNKNOWN_COLOR};

struct Cat {
   char name[MAX_CAT_NAME];
   bool isFixed;
   float weight;
   enum gender catGender;
   enum breed catBreed;
   enum Color collarColor1;
   enum Color collarColor2;
   unsigned long long license;
};

extern struct Cat cat[]; 

extern int numCats;

extern void initializeDatabase();

