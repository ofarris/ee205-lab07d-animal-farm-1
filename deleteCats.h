///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file deleteCats1.h
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   08_03_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern void deleteAllCats();

void deleteCat( int index );
