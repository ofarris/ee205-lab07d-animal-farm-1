///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file addCats.h
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   08_03_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern int isValid(char checkName[],float checkWeight);

extern int addCats(char addName[],enum gender addGender,enum breed addBreed,bool addIsFixed,float addWeight,enum Color addcollarColor1, enum Color addcollarColor2, unsigned long long addlicense);

