///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file updateCats.h
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   08_03_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern int updateCatName(int index, char newName[]);

extern void fixCat(int index);

extern int updateCatWeight(int index, float newWeight);

extern void updateCatCollar1(int index, enum Color newCollarColor1);

extern void updateCatCollar2(int index, enum Color newCollarColor2);

extern void updateLicense(int index, unsigned long long newLicense);






