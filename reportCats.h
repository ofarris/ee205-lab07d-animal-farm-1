///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file reportCats.h
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   08_03_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern char* isFixedToString(bool isFixed);

extern char* genderToString(enum gender theGender);

extern char* breedToString(enum breed theBreed);

extern char* collarToString(enum Color collar);

extern int printCat(int catIndex);

extern int printAllCats();

extern int findCat(char scoutName[]); 



